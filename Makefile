#
# choose compiler and flags to pass to it
#
CXX = g++
DEBUG = -ggdb
STD_CONFORM = -std=c++11
WARNINGS = -Wall -pedantic

#
# internal setup
#
CXXFLAGS = $(DEBUG) $(STD_CONFORM) $(WARNINGS)
LIBS =

#
# files to compile
#
SOURCES = $(wildcard *.cpp)
OBJECTS = $(SOURCES:.cpp=.o)
EXES = $(SOURCES:.cpp=.x)

#
# targets that aren't files-to-be-created
#
.PHONY: all clean

#
# tell make about C++ source and resultant executables
#
.SUFFIXES: .cpp .x

#
# how to compile & link C++ 
#
.cpp.x:
	$(CXX) $(CXXFLAGS) -o $@ $^ $(LIBS)

#
# main targets (default: all)
#
all:	$(EXES)

clean:
	rm -f $(EXES)
