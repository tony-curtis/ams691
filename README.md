SBU AMS 691 C++

C++ source code for practical work

To compile, either type in the g++ command yourself, or "make" to
automate things.

Basic:

  * hello1.cpp
  * hello2.cpp
    * simple "hello world" programs

Debugging:

  * nestednull.cpp
  * null.cpp
    * 2 execution-time bug examples

Style Contrast:

  * walk.cpp
    * contrast C-style pointer-chasing
      
  * iter.cpp
    * with C++ abstractions (compare number of lines of code)

Getting more advanced:

  * string1.cpp
    * string concatenation C++-style
  * add.cpp
    * playing with numeric vectors


Infrastructure:

  * Makefile
    * for compiling these programs with "make"


--

* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)
