//
// requires C++11 support
//

#include <iostream>
#include <vector>
#include <algorithm>
#include <numeric>

static const int SIZE = 10;

static
void
print_element(int n)
{
    std::cout << n << std::endl;
}

int
main(void)
{
    std::vector<int> v(SIZE);

    // fill v with 1 .. SIZE
    std::iota(v.begin(), v.end(), 1);

    // just use a lambda here
    std::for_each(v.begin(), v.end(), [](int& n) {n += 1;});

    // call function
    std::for_each(v.begin(), v.end(), print_element);

    return 0;
}
