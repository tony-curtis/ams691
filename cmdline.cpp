//
// how to pass command-line arguments to the program, run with e.g.
//
//   ./cmdline.x
//
//   ./cmdline.x this is an example
//

#include <iostream>             // provides std::cout, std::endl

int
main(int argc, char *argv[])
{
    std::cout << "Program name is " << argv[0] << std::endl;

    for (int i = 1; i < argc; i += 1) {
        std::cout <<
            "  " <<
            "Argument " << i <<
            " is \"" << argv[i] << "\"" <<
            std::endl;
    }

    return 0;
}
