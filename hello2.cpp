#include <iostream>

//
// how to import routines, but can make life difficult (personally I
// usually avoid doing this)
//
using namespace std;

int
main(void)
{
    cout << "hello world" << endl;

    return 0;
}
