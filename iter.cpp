#include <iostream>
#include <iomanip>
#include <vector>

int
main()
{
    std::vector<float> v;
    std::vector<float>::iterator i;

    v.push_back(4.5);
    v.push_back(3.1415);
    v.push_back(2.818);
    v.push_back(1.0);

    for (i = v.begin(); i < v.end(); i += 1) {
        std::cout <<
            std::fixed <<
            std::setprecision(4) <<
            *i <<
            std::endl;
    }

    return 0;
}
