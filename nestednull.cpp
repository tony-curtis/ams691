//
// demonstrates the "null" pointer bug with a function stack: doing
// "where" in gdb will show the call stack of "f" functions leading
// away from "main".
//

#include <cstddef>
#include <iostream>

void
f4(void)
{
    char *p = nullptr;

    *p = 'X';
}

void
f3(void)
{
    f4();
}

void
f2(void)
{
    f3();
}

void
f1(void)
{
    f2();
}

int
main(void)
{
    f1();

    return 0;
}
