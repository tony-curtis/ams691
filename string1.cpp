//
// how to concatenate strings just with "+" and add new characters,
// walking over the string elements ("cout" would be the sane way of
// outputting strings).  You could use an iterator to e.g. inspect
// individual characters (if you're searching for characters or
// sub-strings, there are other functions you can use).
//
//

#include <iostream>
#include <string>

int
main(void)
{
    std::string s1 = "hello";
    std::string s2 = "world";
    std::string s3;

    s3 = s1 + " " + s2;

    s3.push_back('!');

    std::string::iterator i;

    for (i = s3.begin(); i < s3.end(); i += 1) {
        std::cout << *i << std::endl;
    }

    return 0;
}
