//
// C-style (the older "parent" language of C++) list creation and
// iteration/walking.  Demonstrates the use of pointers, following
// links, and taking structures apart.  C++ has better ways, see
// iter.cpp.
//

#include <stdio.h>
#include <stdlib.h>

typedef struct node node_t;

struct node {
    int item;
    node_t *next;
};

node_t *
new_node(int n, node_t *L)
{

    node_t *h = (node_t *) malloc(sizeof(*h));

    h->item = n;
    h->next = L;

    return h;
}

void
delete_nodes(node_t *L)
{
    if (L != NULL) {
        delete_nodes(L->next);
        free(L);
    }
}

static const char *prefix = " ";
static const unsigned int indent = 4;

static
void
walk_nodes_internal(unsigned int level, node_t *L)
{
    if (L != NULL) {
        printf("%*s%d\n", level, prefix, L->item);
        walk_nodes_internal(level + indent, L->next);
    }
}

void
walk_nodes(node_t *L)
{
    walk_nodes_internal(0, L);
}

node_t *
build_nodes(unsigned int n)
{
    node_t *inner = (n > 0) ? build_nodes(n - 1) : NULL;

    return new_node(n, inner);
}

int
main(void)
{
    node_t *head;

    head = build_nodes(8);

    walk_nodes(head);

    delete_nodes(head);

    return 0;
}
